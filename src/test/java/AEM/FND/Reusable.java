package AEM.FND;


import AEM.Pages.*;
import AEM.utils.JsonDataReader;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.io.Files;
import com.jayway.jsonpath.DocumentContext;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Reusable extends Element {

public static String ENV;
public static String JENKIN;

public static DesiredCapabilities caps;
public static ChromeOptions options = new ChromeOptions();

public Map<String, String> envCapabilities;
public DesiredCapabilities capabilities;
public String compName;
String Device;
static String Website = System.getProperty("WEBSITE");
    public static DocumentContext CONTEXT = JsonDataReader.getDocument("data");

public static String getUrl() {
return JsonDataReader.getAsString(CONTEXT, "$.TESTDATA.URL." + ENV);
}

@BeforeAll
public static void beforeAll() {
ENV = System.getProperty("Env").toUpperCase();
    JENKIN = System.getProperty("JENKIN");
   // OS="Linux";

// systemProperty "RemoteUrl", "http://172.23.83.181:4444/wd/hub";
Optional.ofNullable(System.getProperty("RemoteUrl"))
.filter(s -> !s.isEmpty())
.ifPresent(url -> Configuration.remote = url);
Configuration.startMaximized = true;
caps = new DesiredCapabilities();
caps.setCapability("resolution", "1364x768");
options.addArguments("--no-sandbox");
options.addArguments("--headless");
options.addArguments("--disable-dev-shm-usage");
SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
if(JENKIN.equalsIgnoreCase("true")) {
    System.setProperty("webdriver.chrome.driver", "lib//Windows//chromedriver.exe");
}
else{
    WebDriver driver = new ChromeDriver();

}

open(getUrl());

}
///

@BeforeEach
public void beforeEach() throws IOException {
Configuration.browser = "chrome";
caps.setCapability("resolution", "1364x768");
}

@AfterEach
public void closeBrowser() {
Optional.ofNullable(getWebDriver()).ifPresent(wd -> {
    screenshot();
    wd.quit();
}
);
}

@Attachment(type = "image/png")
public byte[] screenshot() {
try {
File screenshot = Screenshots.takeScreenShotAsFile();
return Files.toByteArray(screenshot);
} catch (IOException e) {
e.printStackTrace();
}
return null;
}


}
