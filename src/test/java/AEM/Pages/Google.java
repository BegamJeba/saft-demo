package AEM.Pages;

import io.qameta.allure.Step;

public class Google extends Element {

    @Step("Verify Google")
    public static void verifyGoogle(){
        google.click();
    }
}
