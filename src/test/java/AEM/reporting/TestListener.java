package AEM.reporting;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.kohsuke.MetaInfServices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@MetaInfServices
public class TestListener implements TestExecutionListener {

    List<Map<String, Object>> testCases = new CopyOnWriteArrayList<>();

    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult
            testExecutionResult) {
        if (testIdentifier.isTest()) {
            System.out.println("Execution finished: " + testIdentifier.getDisplayName() + " " +
                    testExecutionResult.toString());
            String result = testExecutionResult.getStatus().toString();
            if (result == "SUCCESSFUL") {
                result = "pass";
            } else if (result == "FAILED") {
                result = "fail";
            } else {
                result = "unknown";
            }
            String reason = "";
            if (testExecutionResult.getThrowable().isPresent()) {
                reason = testExecutionResult.getThrowable().get().getMessage();
            }
            String suite = "";
            String separator = "class:";
            if (testIdentifier.getParentId().isPresent()) {
                suite = testIdentifier.getParentId().get();
                suite = suite.substring(suite.indexOf(separator) + separator.length(), suite.lastIndexOf("]"));
            }
            Map<String, Object> testCase = new HashMap<String, Object>();
            String name = testIdentifier.getDisplayName();
            if (name.indexOf("(") != -1) {
                name = name.substring(0, name.lastIndexOf("("));
            }
            testCase.put("name", name);
            testCase.put("result", result);
            testCase.put("suite", suite);
            testCase.put("desc", testIdentifier.getDisplayName());
            testCase.put("reason", reason);
            testCases.add(testCase);
        }
    }


    @Override
    public void testPlanExecutionFinished(TestPlan testPlan) {
        if (!testCases.isEmpty()) {
            long pass = testCases.stream().map(s -> s.get("result")).filter("pass"::equals).count();
            long fail = testCases.size() - pass;
            double passPercent = (pass / Double.valueOf(testCases.size())) * 100.00;
            String resultData = String.format("PassPercentage: %.2f%%\nTotalCases: %d | Passed: %d | Failed: %d",
                    passPercent,
                    testCases.size(),
                    pass,
                    fail);
            System.out.println(resultData);
            File resultsFile = new File("results", "percent.txt");
            try {
                resultsFile.getParentFile().mkdirs();
                Files.write(resultsFile.toPath(), resultData.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}