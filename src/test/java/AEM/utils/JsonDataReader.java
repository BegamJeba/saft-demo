package AEM.utils;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class JsonDataReader {

    private static final Logger LOG = Logger.getLogger("JsonDataReader");


    private static final Map<String, DocumentContext> JSON_CONTEXT = new HashMap<>();

    public static DocumentContext getDocument(String type) {

        JSON_CONTEXT.computeIfAbsent(type, JsonDataReader::loadFromFile);
        return JSON_CONTEXT.get(type);
    }

    private static DocumentContext loadFromFile(String type) {
        File file = new File("resources"+File.separator+"testdata", type + ".json");
        LOG.info("Reading Json Data from " + file.getAbsolutePath());
        if (file.exists()) {
            try {
                return JsonPath.parse(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            LOG.severe("File does not exist " + file.getAbsolutePath());
        }
        throw new RuntimeException("Could not load Json Data from - " + file.getAbsolutePath());
    }

    public static String getAsString(DocumentContext context, String path) {
        if (path.contains(" ")) {
            path = Stream.of(path.split("\\.")).map((txt) -> {
                if (txt.contains(" "))
                    return "['" + txt + "']";
                return txt;
            }).collect(Collectors.joining("."));
        }
        return context.read(path, String.class);
    }


}