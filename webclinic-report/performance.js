$.ajaxSetup({
    async: false
});  


    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
          
var params=[];
 for (var i=0;i<url.length;i++) {
          params[i] = url[i].split(",");
   
  }
  var usecasenamedtl=params[0];
  var usecasesortdtl=params[1];


 /*$.getJSON("violations.json", function(json) {
       
         var issuePages=json;
         var hits=json;
         for(var i=0;i<issuePages.length;i++)
         {
            var eachPage=issuePages[i];
          
            for (var j = 0; j < eachPage.length; j++) {
                
                if(eachPage[j].hasOwnProperty('pagename'))
                {
                    //pageName=eachPage[j].pagename;
                  //  alert(eachPage[j].pagename)

                      $('#demoaxe').append('<li><a href= accesibility.html?'+eachPage[j].pagename+' ><i class=fa fa-fw fa-bar-chart-o></i>'+eachPage[j].pagename+'</a></li>');
                }
            }


            

          }
        });*/

     $.getJSON("performance.json", function(json) {
         var hits=json
  var ttfbyte,totalpage_time,domComplete,downloadTime,initialConnection,domLoadingtoComplete,domLoadingtoLoaded,cache_ttfbyte,cache_totalpage_time,cache_domComplete,cache_downloadTime,cache_initialConnection,cache_domLoadingtoComplete,cache_domLoadingtoLoaded;
var dataMorrisArray=[];
            //Separating Usecase wise details
            
            for(var i=0;i<hits.length;i++)
            {

                //alert(hits[i].PageName);
        
               $('#demo').append('<li><a href= performance.html?'+hits[i].PageName+' ><i class=fa fa-fw fa-bar-chart-o></i>'+hits[i].PageName+'</a></li>');


            }

//alert(hits.length)

              for(var i=0;i<hits.length;i++)
            {   

                var usecaseame=hits[i].PageName;

                ttfbyte=hits[i].TimeToFirstByte;
                totalpage_time=hits[i].TotalLoadTime;
                domComplete=hits[i].DomComplete;
                downloadTime=hits[i].DownloadTime;
                initialConnection=hits[i].InitialConnection;
                domLoadingtoComplete=hits[i].DOMLoadingtoComplete;
                domLoadingtoLoaded=hits[i].DOMLoadingtoLoaded;
               // dataMorrisArray.push(totalpage_time); 
               dataMorrisArray.push( {
    "x": "LoadTime-"+usecaseame,
    "y": totalpage_time
  } );
               // alert(dataMorrisArray)
            
                if(usecaseame==usecasenamedtl)  
                   
                {




                    if(totalpage_time<3000)
                {
                document.getElementById('launchpanel').className+= 'panel panel-green';
                }
                else if((totalpage_time>3000)&&(totalpage_time<=5000))
                {
                document.getElementById('launchpanel').className+= 'panel panel-yellow';
                 }
                else if(totalpage_time>5000)
                {
                document.getElementById('launchpanel').className+= 'panel panel-red';
                }
                document.getElementById("pagetime").innerHTML=totalpage_time+"ms";



                var chart = AmCharts.makeChart("chartresponse", {
    "type": "serial",
    "theme": "No theme",
    "fillEmptyCategories": true,
    "legend": {
    "align": "center",
    "periodValueText": "total: [[value.sum]]",
    "valueText": "[[value]] ([[percents]]%)",
    /*"valueWidth": 100,*/
        "autoMargins": false,
        "borderAlpha": 0.2,
        "equalWidths": true,
        "horizontalGap": 10,
        "markerSize": 10,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 0,
        "position": "right"
    },
    "dataProvider": [{
        "year": "Chrome w/o Cache",
        "InitialConnection": initialConnection,
        "TimetoFirstByte": ttfbyte,
        "DownloadTime": downloadTime,
        "DomContentLoaded" :domLoadingtoLoaded,
        "DOMLoadingtoComplete":domLoadingtoComplete
    }],
    "valueAxes": [{
        "stackType": "100%",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "labelsEnabled": false,
        "position": "left"
    }],
    "graphs": [ {
        "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        "fillAlphas": 0.9,
        "fontSize": 11,
        "fixedColumnWidth": 100,
        "labelText": initialConnection +"ms",
        "lineAlpha": 0.5,
        "title": "InitialConnection",
        "type": "column",
        "valueField": "InitialConnection",
        "index":1
    },{
        "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        "fillAlphas": 0.9,
        "fontSize": 11,
        "labelText": ttfbyte +"ms",
        "lineAlpha": 0.5,
       "fixedColumnWidth": 100,
        "title": "TimetoFirstByte",
        "type": "column",
        "valueField": "TimetoFirstByte",
        "index":2
    }, {
        "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;color:#0B610B;'><b>[[value]]</b> ([[percents]]%)</span>",
        "fillAlphas": 0.9,
        "fontSize": 11,
        "labelText": downloadTime +"ms",
        "lineAlpha": 0.5,
        "fixedColumnWidth": 100,
        "title": "DownloadTime",
        "type": "column",
        "valueField": "DownloadTime",
        "index":3
    }, {
        "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        "fillAlphas": 0.9,
        "fontSize": 11,
        "labelText": domLoadingtoLoaded +"ms",
        "lineAlpha": 0.5,
         "fixedColumnWidth": 100,
        "title": "DomContentLoaded",
        "type": "column",
        "valueField": "DomContentLoaded",
        "index":5
    }, {
        "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
        "fillAlphas": 0.9,
        "fontSize": 11,
        "labelText": domLoadingtoComplete +"ms",
        "lineAlpha": 0.5,
        "fixedColumnWidth": 100,
        "title": "DOMLoadingtoComplete",
        "type": "column",
        "valueField": "DOMLoadingtoComplete",
        "index":6
    }],
    "marginTop": 30,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 40,
    "autoMargins": false,
    "categoryField": "year",
    "categoryAxis": {
        "categoryAxis.dashLength":100,
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0
    },
    "export": {
        "enabled": false
     }

});


}
}

AmCharts.addInitHandler(function(chart) {

  var dataProvider = chart.dataProvider;
  var colorRanges = chart.colorRanges;

  // Based on https://www.sitepoint.com/javascript-generate-lighter-darker-color/
  function ColorLuminance(hex, lum) {

    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    var rgb = "#",
      c, i;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ("00" + c).substr(c.length);
    }

    return rgb;
  }

  if (colorRanges) {

    var item;
    var range;
    var valueProperty;
    var value;
    var average;
    var variation;
    for (var i = 0, iLen = dataProvider.length; i < iLen; i++) {

      item = dataProvider[i];

      for (var x = 0, xLen = colorRanges.length; x < xLen; x++) {

        range = colorRanges[x];
        valueProperty = range.valueProperty;
        value = item[valueProperty];

        if (value >= range.start && value <= range.end) {
          average = (range.start - range.end) / 2;

          if (value <= average)
            variation = (range.variation * -1) / value * average;
          else if (value > average)
            variation = range.variation / value * average;

          item[range.colorProperty] = ColorLuminance(range.color, variation.toFixed(2));
        }
      }
    }
  }

}, ["serial"]);


  var chart = AmCharts.makeChart("barchart", {
    "theme": "light",
    "type": "serial",
    "colorRanges": [{
    "start": 0,
    "end": 3000,
    "color": "#B0DE09",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  },{
    "start": 3001,
    "end": 5000,
    "color": "#04D215",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  }, {
    "start": 5001,
    "end": 7000,
    "color": "#FF9E01",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  }, {
    "start": 7001,
    "end": 9000,
    "color": "#8A0CCF",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  }, {
    "start": 9001,
    "end": 12000,
    "color": "#0D8ECF",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  },{
    "start": 12001,
    "end": 50000,
    "color": "#FF0F00",
    "variation": 0,
    "valueProperty": "y",
    "colorProperty": "color"
  }],

   "dataProvider":dataMorrisArray,
   
    "valueAxes": [{
        "gridColor": "#FFFFFF",
    "gridAlpha": 0.2,
    "dashLength": 0
    }],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        /*"fillColorsField": "color",*/
        "fillAlphas": 0.8,
        "lineAlpha": 0.2,
        "openField": "start",
        "type": "column",
        "valueField": "end",
        "columnSpacing": 0,
        "valueField": "y",
        "columnWidth":0,
        "fixedColumnWidth": 100,
        "colorField": "color"
    }],
    "depth3D": 20,
  "angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "x",
    
    "tickPosition": "start",
    "categoryAxis": {
       /*"axisAlpha": 0,
    "gridAlpha": 0,*/
        "gridPosition": 0,
        "gridThickness": 0,
        "labelRotation": 0
    },
    "export": {
      "enabled": false
     }

});


});



     $.getJSON("log.json", function(json) {


var hits=json;
var image,css,other,xhr,nwbmp,runtype,pagesize,html,js,font,video,cachenwbmp,cachepagesize,s;
var harpath;



for(var i=0;i<hits.length;i++)
{

 var usecaseame=hits[i].pagename;

        image=hits[i].image;
        css=hits[i].css;
        other=hits[i].other;
        xhr=hits[i].xhr;
        nwbmp=hits[i].nwrequest;
        //alert(nwbmp)
        pagesize=hits[i].pagesize;
        video=hits[i].video;
        html=hits[i].html;
        js=hits[i].javascript;
        font=hits[i].font;
        harpath=hits[i].harpath;
//alert(usecaseame==usecasenamedtl)
 if(usecaseame==usecasenamedtl)  
                   
                {

                    s=(pagesize/(1024*1024)).toFixed(2);


                    

                    if(nwbmp<50)
                {
                document.getElementById('launchpanel2').className+= 'panel panel-green';
                }
                else if((nwbmp>50)&&(nwbmp<=60))
                {
                document.getElementById('launchpanel2').className+= 'panel panel-yellow';
            }
                else if(nwbmp>60)
                {
                document.getElementById('launchpanel2').className+= 'panel panel-red';
                }
                document.getElementById("mwcall").innerHTML=nwbmp;



               if(s<=1.5)
                {
                document.getElementById('launchpanel1').className+= 'panel panel-green';
                }
                else if((s>1.5)&&(s<=2))
                {
                document.getElementById('launchpanel1').className+= 'panel panel-yellow';
                }
                else if(s>2)
                {
                document.getElementById('launchpanel1').className+= 'panel panel-red';
                }
                document.getElementById("pagesize").innerHTML=s+"MB";



if(nwbmp<50)
{

document.getElementById("harappend").appendChild(document.body.appendChild(div("data-har-url", relativeToAbsolute(harpath), "300px")));
}
else if(nwbmp>50)
{

document.getElementById("harappend").appendChild(document.body.appendChild(div("data-har-url", relativeToAbsolute(harpath), "550px")));
}



function relativeToAbsolute(url) {
  if (url.indexOf("//") > -1) {
    return url;
  }
  // trick using <a> element to turn relative URLs into absolute.
  var a = document.createElement("a");
  a.href = url;
  return a.href;
}
function div(attr, url, height) {
  var div = document.createElement("div");
  div.className = "har";
  div.setAttribute(attr, url);
  div.setAttribute("height", height);
  return div;
}


var googleAnalyticsProfile = "UA-82884352-1";
if (googleAnalyticsProfile && googleAnalyticsProfile.charAt(0) !== "@") {

    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
    ga('create', googleAnalyticsProfile, 'auto');
    ga('send', 'pageview');

}

                var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "dark",
  "dataProvider": [ {
    "PageProfiling": "CSS",
    "Values": css
  }, {
    "PageProfiling": "Font",
    "Values": font
  }, {
    "PageProfiling": "HTML",
    "Values": html
  }, {
    "PageProfiling": "Images",
    "Values": image
  }, {
    "PageProfiling": "JS",
    "Values": js
  }, {
    "PageProfiling": "Video",
    "Values": video
  }, {
    "PageProfiling": "Other",
    "Values": other
  }, {
    "PageProfiling": "XHR",
    "Values": xhr
  } ],
  "valueField": "Values",
  "titleField": "PageProfiling",
   "balloon":{
   "fixedPosition":true
  },
  "export": {
   "enabled": true,
    "menu": []
  }
  
} );


 }

}

     });

     function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
 


     $.getJSON("recommendations.json", function(json) {


var hits=json;

var recommendations,pagename;

var rule,message, suggest;
//alert(hits.length)

var row=document.createElement('h3');
                            row.setAttribute("style","color:#3c9adc");

                          


for(var i=0;i<hits.length;i++)
{

pagename=hits[i].pagename;


//alert(pagename==usecasenamedtl)

if(pagename==usecasenamedtl)

{

recommendations=hits[i].recommendation;



/*for(j=0;j<recommendations.length;j++)

{
  rule=recommendations[j].ruleHeader;

    var ul=document.createElement('ul');
                            ul.setAttribute("style","font-weight:normal;line-height:0,letter-spacing: normal;font-family:Arial;text-align:left;font-size:20px");
                            ul.innerText=rule;

  var li1=document.createElement('li'); 
                            li1.setAttribute("style","color:#808080"); 
// alert(rule)
//li1.innerText=rule;
//ul.appendChild(li1);
row.appendChild(ul);

document.getElementById("rec").appendChild(row);
}

*/
for(j=0;j<recommendations.length;j++)

{
  rule=recommendations[j].ruleHeader;
  message=recommendations[j].Message;
  suggest=recommendations[j].Recommendation;

    var ul=document.createElement('ul');
                            ul.setAttribute("style","font-weight:normal;line-height:0,letter-spacing: normal;font-family:Arial;text-align:left;font-size:20px");
                            ul.setAttribute("id",j);
                            ul.setAttribute("class","accordion")
                            ul.innerHTML='<b>'+rule+'</b>';

  var li1=document.createElement('li'); 
                            li1.setAttribute("style","color:#808080"); 
                            var a=document.createElement('a');
                            a.setAttribute("class","fa fa-hand-o-right")

                       
var msgurl=message;
msgurl=msgurl.toString().replace(/[\[\]']+/g,'').replace(/\,/g,'').replace(/\n/g, '\\n');;
li1.innerText=suggest;


a.setAttribute("onclick","getUrl('"+msgurl+"','"+j+"');");
//a.innerHTML="more info";


ul.appendChild(li1);
ul.appendChild(a);
row.appendChild(ul);


}

}

}

document.getElementById("rec").appendChild(row);

});

     function getUrl(msgurl,j) {

    //alert("inspired "+j)
       //alert(msgurl)
    var ul = document.getElementById(j);
   // var content=msgurl.replace(/\'/g,'');


if(ul!=undefined)
{
$("#"+"expandlinks").remove();

}


                      var li2=document.createElement('li'); 
                    li2.setAttribute("style","color:#808080;list-style-type: circle;margin-left: 50px"); 
                     li2.setAttribute("id","expandlinks")
                    // var fix=content.replace("\\u2022/g", "/\n/g");
                    
                    li2.innerText=msgurl;
                    ul.appendChild(li2);
                      
                        
                        

}  

      $.getJSON("response.json", function(json) {


var hits=json;
var response,pageName;

for(var i=0;i<hits.length;i++)

{

response=hits[i].responsetime;
pageName=hits[i].pagename;

if(pageName==usecasenamedtl)

{



                    if(response<3000)
                {
                document.getElementById('launchpanel11').className+= 'panel panel-green';
                 document.getElementById("javatime").innerHTML=response+"ms";
                }
                else if((response>3000)&&(response<=5000))
                {
                document.getElementById('launchpanel11').className+= 'panel panel-yellow';
                 document.getElementById("javatime").innerHTML=response+"ms";
                 }
                else if(response>5000)
                {
                document.getElementById('launchpanel11').className+= 'panel panel-red';
                 document.getElementById("javatime").innerHTML=response+"ms";
                }
               

//document.getElementById("javatime").innerHTML=response;

}


}

});